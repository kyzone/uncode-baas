package cn.uncode.baas.server.internal.module.data;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import cn.uncode.dal.utils.JsonUtils;
import cn.uncode.baas.server.cache.SystemCache;
import cn.uncode.baas.server.dto.RestApp;
import cn.uncode.baas.server.internal.context.RestContextManager;

public class DataParamResolve implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -1214745052234402202L;

    public static final String FIELD_TABLE = "table";
    public static final String FIELD_FIELDS = "fields";
    public static final String FIELD_TIME_OUT = "timeout";
    public static final String FIELD_OBJECT_ID = "id";

    private List<String> fields;
    private Map<String, Object> params;
    private String database;
    private String table;
    private int seconds;
    private String id;

    public DataParamResolve(Map<String, Object> param) {
        if (param != null && param.size() > 0) {
            if (param.containsKey(FIELD_TABLE)) {
                RestApp restApp = SystemCache.getRestApp(RestContextManager.getContext().getBucket());
                this.database = restApp.getDatabase();
                this.table = String.valueOf(param.get(FIELD_TABLE));
                param.remove(FIELD_TABLE);
            }
            if (param.containsKey(FIELD_FIELDS)) {
                String value = String.valueOf(param.get(FIELD_FIELDS));
                if (StringUtils.isNotEmpty(value)) {
                    fields = Arrays.asList(value.split(","));
                }
                param.remove(FIELD_FIELDS);
            }
            if (param.containsKey(FIELD_TIME_OUT)) {
                String value = String.valueOf(param.get(FIELD_TIME_OUT));
                if (StringUtils.isNotEmpty(value)) {
                    double to = Double.valueOf(value);
                    seconds = (int) to;
                }
                param.remove(FIELD_TIME_OUT);
            }
            if (param.containsKey(FIELD_OBJECT_ID)) {
                String value = String.valueOf(param.get(FIELD_OBJECT_ID));
                if (StringUtils.isNotEmpty(value)) {
                    id = value;
                }
                param.remove(FIELD_OBJECT_ID);
            }
            params = (Map<String, Object>) param;
        }
    }

    public String getMongoScript() {
        String mongoScript = null;
        if (params != null) {
            mongoScript = JsonUtils.objToJson(params);
        }
        return mongoScript;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public Map<String, Object> getParams(List<String> hiddenFields) {
        if (hiddenFields != null) {
            for (String field : hiddenFields) {
                if (params.containsKey(field)) {
                    params.remove(field);
                }
            }
        }
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public int getId() {
        return Integer.valueOf(id);
    }

    public String getObjectId() {
        return id;
    }

}
