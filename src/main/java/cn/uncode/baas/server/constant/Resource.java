package cn.uncode.baas.server.constant;

public interface Resource {

    /**
     * jsonp方法参数名
     */
    public static final String RESP_CALLBACK = "callback";

    public static final String REQ_METHOD_GET = "GET";

    public static final String REQ_METHOD_POST = "POST";

    public static final String REQ_METHOD_POST_LIST = "POST_LIST";

    public static final String REQ_METHOD_DELETE = "DELETE";

    public static final String REQ_METHOD_PUT = "PUT";

    public static final String REQ_ACCESS_TOKEN = "token";

    /**
     * 消除缓存类型
     */
    public static final String CLEAR_CACHE_TYPE_RESTER = "rester";
    public static final String CLEAR_CACHE_TYPE_TABLE = "table";

    /**
     * 默认版本标识
     */
    public static final String REQ_VERSION_DEFAULT = "df";

    public static final int PAGE_INDEX = 1;

    public static final int PAGE_SIZE = 20;

    /**
     * js空值
     */
    public static final String SCRIPT_VALUE_NULL = "null";

    /**
     * 数据源类型
     */
    public static final int REST_APP_DB_TYPE_DEFAULT = 0;
    public static final int REST_APP_DB_TYPE_MYSQL = 1;
    public static final int REST_APP_DB_TYPE_MONGO = 2;

    /**
     * 正常
     */
    public static final int REST_APP_STATUS_ENABLE = 1;
    /**
     * 禁用
     */
    public static final int REST_APP_STATUS_DISABLE = 0;

    /**
     * 系统用户相关字段
     */
    public static final String REST_USER_USERNAME = "username";
    public static final String REST_USER_PASSWORD = "password";
    public static final String REST_USER_STATUS = "status";
    public static final String REST_USER_OLD_PASSWORD = "oldPassword";
    public static final String REST_USER_EMAIL = "email";
    public static final String REST_USER_MOBILE = "mobile";
    public static final String REST_USER_EMAIL_TYPE = "emailType";

    public static final int REST_USER_STATUS_INVALID = 0;
    public static final int REST_USER_STATUS_VALID = 1;
    /* 注册时需要邮箱认证 */
    public static final int REST_USER_EMAIL_AUTH_YES = 1;
    /* 注册时不需要认证 */
    public static final int REST_USER_EMAIL_AUTH_NO = 0;

    public static final String REQ_EMAIL_CODE = "code";

    public static final String REQ_USER_LINK_KYE = "link";

    /**
     * token过期时间-秒
     */
    public static final int ACCESS_TOKEN_EXPIRATION_TIME = 30 * 60;

    /**
     * token key 前缀
     */
    public static final String ACCESS_TOKEN_CACHE_KEY_PREFIX = "access_token_";

    /**
     * 任意参数
     */
    public static final int REST_METHOD_PARAM_TYPE_ANY = 1;
    /**
     * 自定义参数
     */
    public static final int REST_METHOD_PARAM_TYPE_CUSTOM = 2;
    /**
     * 无任何参数
     */
    public static final int REST_METHOD_PARAM_TYPE_NOTHING = 3;

    public static final String REST_DEFAULT_DATABASE = "rest";

    /**
     * 管理后台多对一、一对多关系字段
     */
    public static final String REST_TABLE_MANY_TO_ONE_TABLE_KEY = "table";
    public static final String REST_TABLE_MANY_TO_ONE_FIELD_KEY = "field";
    public static final String REST_TABLE_MANY_TO_ONE_FIELD2_KEY = "field2";
    public static final String REST_TABLE_MANY_TO_ONE_DISPLAY_KEY = "display";
    public static final String REST_TABLE_MANY_TO_ONE_ORDER_KEY = "order";
    public static final String REST_TABLE_ONE_TO_MANY_ONE_KEY = "one";
    public static final String REST_TABLE_ONE_TO_MANY_JOIN_KEY = "join";
    public static final String REST_TABLE_ONE_TO_MANY_MANY_KEY = "many";
    public static final String REST_TABLE_ONE_TO_MANY_DESC_KEY = "desc";

    public static final String REST_SYSTEM_BUCKET_NAME = "system";
    
    public static final String REST_SYSTEM_ADMIN_RESOURCES_COUNT_RESULT_KEY = "count";
    
    
    public static final String 	TABLE_OPTION_READ = "read";
    public static final String 	TABLE_OPTION_INSERT = "insert";
    public static final String 	TABLE_OPTION_UPDATE = "update";
    public static final String 	TABLE_OPTION_REMOVE = "remove";
    
    
    
    public static final String REST_GROUP_NAMES_KEY = "names";
    

    /**
     * 使用此请求头提供用户名密码。
     */
    public static final String SIMPLE_AUTH_HEADER = "X-Auth";

    /**
     * 使用此请求头提供请求的时间戳。格式为毫秒数。
     */
    public static final String SIMPLE_AUTH_TIMESTAMP_HEADER = "X-Auth-Timestamp";

}
